defmodule PlugMultipartRelated.MixProject do
  use Mix.Project

  def project do
    [
      app: :plug_multipart_related,
      version: "0.1.1",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "CHANGELOG*", "LICENSE.txt"],
      licenses: ["MIT"],
      links: %{"BitBucket" => "https://bitbucket.org/mhvdboog/plug_multipart_related/src/master"}
    ]
  end

  defp description do
    "This is a partial implementation of RFC2387. This implementation is heavily influence by `Plug.Parsers.MULTIPART` which is shipped with Plug."
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:plug, "~> 1.0"}
    ]
  end
end
