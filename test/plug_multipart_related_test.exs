defmodule Plug.Parsers.RELATED.Test do
  use ExUnit.Case
  doctest Plug.Parsers.RELATED

  use Plug.Test

  def parse(conn, opts \\ []) do
    opts = Keyword.put_new(opts, :parsers, [Plug.Parsers.RELATED])
    Plug.Parsers.call(conn, Plug.Parsers.init(opts))
  end

  test "complex multipart related bodies" do
    multipart = """
    --MIME_boundary\r
    Content-Type: application/xml\r
    Content-Disposition: attachment; filename=as4_header.ml\r
    Content-ID: <soap-header>\r
    \r
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:eb="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/">
      <soap:Header>
        <eb:Messaging soap:mustUnderstand="true">
          <eb:UserMessage>
            <eb:MessageInfo>
              <eb:Timestamp>2018-10-17T03:51:16Z</eb:Timestamp>
              <eb:MessageId>9ed644ee-5c62-4583-8b46-fc094304d576</eb:MessageId>
            </eb:MessageInfo>
            <eb:PartyInfo>
              <eb:From>
                <eb:PartyId type="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">dcaf-sender</eb:PartyId>
                <eb:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultRole</eb:Role>
              </eb:From>
              <eb:To>
                <eb:PartyId type="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">15613331838</eb:PartyId>
                <eb:Role>http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultRole</eb:Role>
              </eb:To>
            </eb:PartyInfo>
            <eb:CollaborationInfo>
              <eb:Service>dbc-procid::urn:resources.digitalbusinesscouncil.com.au:dbc:einvoicing:ver1.0</eb:Service>
              <eb:Action>dbc-docid::urn:resources.digitalbusinesscouncil.com.au:dbc:invoicing:documents:core-invoice:xsd::core-invoice-1##urn:resources.digitalbusinesscouncil.com.au:dbc:einvoicing:process:einvoicing01:ver1.0</eb:Action>
              <eb:ConversationId>6837fffb-ab22-4d55-9553-9a4bccc65ccd</eb:ConversationId>
            </eb:CollaborationInfo>
            <eb:MessageProperties>
              <eb:Property name="originalSender" type="test-id">supplier</eb:Property>
              <eb:Property name="finalRecipient" type="test-id">buyer</eb:Property>
            </eb:MessageProperties>
            <eb:PayloadInfo>
              <eb:PartInfo href="cid:invoice">
                <eb:PartProperties>
                  <eb:Property name="MimeType">application/xml</eb:Property>
                </eb:PartProperties>
              </eb:PartInfo>
            </eb:PayloadInfo>
          </eb:UserMessage>
        </eb:Messaging>
      </soap:Header>
      <soap:Body/>
    </soap:Envelope>
    \r
    --MIME_boundary\r
    Content-Type: text/plain\r
    Content-ID: <test_message>\r
    \r
    \r
    This is merely some text.
    \r
    --MIME_boundary\r
    Content-Type: application/xml\r
    Content-Disposition: attachment; filename=ubl_invoice.xml\r
    Content-ID: <invoice>\r
    \r
    <?xml version="1.0" encoding="UTF-8"?>
    <!--Sample XML file with examples of possible values for a conformant eInvoice -->
    <n2:Invoice xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:n1="http://www.altova.com/samplexml/other-namespace"
      xmlns:n2="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"
      xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
      xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
      xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xsi:schemaLocation="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 file:../xsd/maindoc/CoreInvoice-2.0.xsd">
      <cbc:UBLVersionID>2.1</cbc:UBLVersionID>
      <cbc:CustomizationID schemeAgencyID="dbc">urn:resources.digitalbusinesscouncil.com.au:dbc:invoicing:documents:core-invoice:xsd::core-invoice-1##urn:resources.digitalbusinesscouncil.com.au:dbc:einvoicing:process:einvoicing01:ver1.0</cbc:CustomizationID>
      <cbc:ProfileID schemeAgencyID="dbc">urn:resources.digitalbusinesscouncil.com.au:dbc:einvoicing:ver1.0</cbc:ProfileID>
      <cbc:ID>TOSL-108-A</cbc:ID>
      <cbc:IssueDate>2016-07-01</cbc:IssueDate>
      <cbc:DueDate>2016-08-01</cbc:DueDate>
      <cbc:InvoiceTypeCode listName="Document Type Code" listAgencyID="6">388</cbc:InvoiceTypeCode>
      <cbc:DocumentCurrencyCode listID="ISO 4217 Alpha" listAgencyID="5">AUD</cbc:DocumentCurrencyCode>
      <cbc:BuyerReference>CC-3352626</cbc:BuyerReference>
      <cac:InvoicePeriod>
        <cbc:StartDate>2016-05-01</cbc:StartDate>
        <cbc:EndDate>2016-06-01</cbc:EndDate>
      </cac:InvoicePeriod>
      <cac:OrderReference>
        <cbc:ID>SB002</cbc:ID>
      </cac:OrderReference>
      <cac:AccountingSupplierParty>
        <cac:Party>
          <cac:PartyIdentification>
            <cbc:ID schemeID="urn:oasis:names:tc:ebcore:partyid-type:iso6523:088" schemeAgencyID="GS1">4035811611014</cbc:ID>
          </cac:PartyIdentification>
          <cac:PartyName>
            <cbc:Name>ACME Holdings</cbc:Name>
          </cac:PartyName>
          <cac:PostalAddress>
            <cbc:CityName>Adelaide</cbc:CityName>
            <cbc:PostalZone>5000</cbc:PostalZone>
            <cbc:CountrySubentity>South Australia</cbc:CountrySubentity>
            <cac:AddressLine>
              <cbc:Line>88 Grenfell St</cbc:Line>
            </cac:AddressLine>
            <cac:Country>
              <cbc:IdentificationCode listName="Country Identification Code" listAgencyID="5">AU</cbc:IdentificationCode>
            </cac:Country>
          </cac:PostalAddress>
          <cac:PartyLegalEntity>
            <cbc:CompanyID schemeID="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">987654321</cbc:CompanyID>
          </cac:PartyLegalEntity>
        </cac:Party>
      </cac:AccountingSupplierParty>
      <cac:AccountingCustomerParty>
        <cac:Party>
          <cac:PartyIdentification>
            <cbc:ID schemeID="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">51083392303</cbc:ID>
          </cac:PartyIdentification>
          <cac:PartyName>
            <cbc:Name>Governmment Agency</cbc:Name>
          </cac:PartyName>
          <cac:PostalAddress>
            <cbc:CityName>Willunga</cbc:CityName>
            <cbc:PostalZone>5172</cbc:PostalZone>
            <cbc:CountrySubentity>South Australia</cbc:CountrySubentity>
            <cac:AddressLine>
              <cbc:Line>Delabole Road</cbc:Line>
            </cac:AddressLine>
            <cac:Country>
              <cbc:IdentificationCode listName="Country Identification Code" listAgencyID="5">AU</cbc:IdentificationCode>
            </cac:Country>
          </cac:PostalAddress>
          <cac:PartyLegalEntity>
            <cbc:CompanyID schemeID="urn:oasis:names:tc:ebcore:partyid-type:iso6523:0151">51083392303</cbc:CompanyID>
          </cac:PartyLegalEntity>
        </cac:Party>
        <cac:BuyerContact>
          <cbc:ID>Tony Curtis</cbc:ID>
          <cbc:Telephone>(08) 8556 2345</cbc:Telephone>
          <cbc:ElectronicMail>curtis@willunga.gov.au</cbc:ElectronicMail>
        </cac:BuyerContact>
      </cac:AccountingCustomerParty>
      <cac:Delivery>
        <cbc:ActualDeliveryDate>2016-03-02</cbc:ActualDeliveryDate>
        <cac:DeliveryAddress>
          <cbc:CityName>Adelaide</cbc:CityName>
          <cbc:PostalZone>5000</cbc:PostalZone>
          <cbc:CountrySubentity>South Australia</cbc:CountrySubentity>
          <cac:AddressLine>
            <cbc:Line>202 North Terrace</cbc:Line>
          </cac:AddressLine>
          <cac:Country>
            <cbc:IdentificationCode listName="Country Identification Code" listAgencyID="5">AU</cbc:IdentificationCode>
          </cac:Country>
        </cac:DeliveryAddress>
      </cac:Delivery>
      <cac:PaymentMeans>
        <cbc:ID>EFT</cbc:ID>
        <cbc:PaymentMeansCode listID="UN/ECE 4461">2</cbc:PaymentMeansCode>
        <cbc:InstructionID>888276612262653</cbc:InstructionID>
        <cac:PayeeFinancialAccount>
          <cbc:ID>2000987211</cbc:ID>
          <cac:FinancialInstitutionBranch>
            <cbc:ID schemeName="BSB">086016</cbc:ID>
            <cbc:Name>NAB Adelaide</cbc:Name>
          </cac:FinancialInstitutionBranch>
        </cac:PayeeFinancialAccount>
      </cac:PaymentMeans>
      <cac:TaxTotal>
        <cbc:TaxAmount currencyID="AUD">250.00</cbc:TaxAmount>
      </cac:TaxTotal>
      <cac:LegalMonetaryTotal>
        <cbc:LineExtensionAmount currencyID="AUD">2500.00</cbc:LineExtensionAmount>
        <cbc:TaxExclusiveAmount currencyID="AUD">2500.00</cbc:TaxExclusiveAmount>
        <cbc:TaxInclusiveAmount currencyID="AUD">2750.00</cbc:TaxInclusiveAmount>
        <cbc:PayableAmount currencyID="AUD">2750.00</cbc:PayableAmount>
      </cac:LegalMonetaryTotal>
      <cac:InvoiceLine>
        <cbc:ID>1</cbc:ID>
        <cbc:Note>A variety of Widgets</cbc:Note>
        <cbc:InvoicedQuantity>200</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="AUD">2000.00</cbc:LineExtensionAmount>
        <cac:TaxTotal>
          <cbc:TaxAmount currencyID="AUD">200.00</cbc:TaxAmount>
          <cac:TaxSubtotal>
            <cbc:TaxAmount currencyID="AUD">10.00</cbc:TaxAmount>
            <cac:TaxCategory>
              <cac:TaxScheme>
                <cbc:ID>GST</cbc:ID>
              </cac:TaxScheme>
            </cac:TaxCategory>
          </cac:TaxSubtotal>
        </cac:TaxTotal>
        <cac:Item>
          <cbc:Description>Widget</cbc:Description>
          <cac:SellersItemIdentification>
            <cbc:ID>WDGT-A1733-0436</cbc:ID>
          </cac:SellersItemIdentification>
          <cac:StandardItemIdentification>
            <cbc:ID schemeID="GTIN" schemeAgencyID="GS1">9501101021037</cbc:ID>
          </cac:StandardItemIdentification>
        </cac:Item>
        <cac:Price>
          <cbc:PriceAmount currencyID="AUD">10.00</cbc:PriceAmount>
          <cbc:BaseQuantity>1</cbc:BaseQuantity>
        </cac:Price>
      </cac:InvoiceLine>
      <cac:InvoiceLine>
        <cbc:ID>2</cbc:ID>
        <cbc:Note>Widget attachments</cbc:Note>
        <cbc:InvoicedQuantity>200</cbc:InvoicedQuantity>
        <cbc:LineExtensionAmount currencyID="AUD">500.00</cbc:LineExtensionAmount>
        <cac:TaxTotal>
          <cbc:TaxAmount currencyID="AUD">50.00</cbc:TaxAmount>
          <cac:TaxSubtotal>
            <cbc:TaxAmount currencyID="AUD">10.00</cbc:TaxAmount>
            <cac:TaxCategory>
              <cac:TaxScheme>
                <cbc:ID>GST</cbc:ID>
              </cac:TaxScheme>
            </cac:TaxCategory>
          </cac:TaxSubtotal>
        </cac:TaxTotal>
        <cac:Item>
          <cbc:Description>Widget screws</cbc:Description>
          <cac:SellersItemIdentification>
            <cbc:ID>WDGT-A1733-0437</cbc:ID>
          </cac:SellersItemIdentification>
          <cac:StandardItemIdentification>
            <cbc:ID schemeID="GTIN" schemeAgencyID="GS1">9501101021038</cbc:ID>
          </cac:StandardItemIdentification>
        </cac:Item>
        <cac:Price>
          <cbc:PriceAmount currencyID="AUD">2.50</cbc:PriceAmount>
          <cbc:BaseQuantity>1</cbc:BaseQuantity>
        </cac:Price>
      </cac:InvoiceLine>
    </n2:Invoice>
    \r
    --MIME_boundary--\r
    """


    %{params: params} =
      conn(:post, "/", multipart)
      |> put_req_header("content-type", "multipart/related; type=\"application/soap+xml\"; boundary=MIME_boundary")
    |> parse()

    assert Enum.count(params) == 3

  end

  test "parses multipart related bodies with test body" do
    multipart = """
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name=\"pic\"; filename=\"foo.txt\"\r
    Content-ID: test_id\r
    Content-Type: text/plain\r
    \r
    hello
    \r
    ------w58EW1cEpjzydSCq\r
    Content-ID: unique\r
    \r
    skipped\r
    ------w58EW1cEpjzydSCq\r
    Content-ID: <soap_header>\r
    Content-Disposition: attachment; filename=as4_header.xml\r
    Content-Type: application/soap+xml\r
    \r
    hello\r
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name=\"empty\"; filename=\"\"\r
    Content-Type: application/octet-stream\r
    \r
    \r
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name="status[]"\r
    \r
    choice1\r
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name="status[]"\r
    \r
    choice2\r
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name=\"commit\"\r
    \r
    Create User\r
    ------w58EW1cEpjzydSCq--\r
    """


    %{params: params} =
      conn(:post, "/", multipart)
      |> put_req_header("content-type", "multipart/related; type=\"application/soap+xml\"; boundary=----w58EW1cEpjzydSCq; start=\"<soap_header>\"")
      |> parse()

    assert Enum.count(params) == 3

    assert params["<soap_header>"]["start"] == true

    assert %Plug.Upload{} = file = params["<soap_header>"]["body"]
    assert File.read!(file.path) == "hello"
    assert file.content_type == "application/soap+xml"
    assert file.filename == "as4_header.xml"
  end

  test "parses multipart related bodies with a small test" do
    multipart = """
    ------w58EW1cEpjzydSCq\r
    Content-Disposition: form-data; name=\"info\"; filename=\"foo.txt\"\r
    Content-ID: <part1>\r
    Content-Type: text/plain\r
    \r
    bar
    \r
    ------w58EW1cEpjzydSCq\r
    Content-ID: <part2>\r
    \r
    foobar\r
    ------w58EW1cEpjzydSCq\r
    Content-Type: text/plain\r
    \r
    ignored\r
    ------w58EW1cEpjzydSCq--\r
    """


    %{params: params} =
      conn(:post, "/", multipart)
      |> put_req_header("content-type", "multipart/related; type=\"application/soap+xml\"; boundary=----w58EW1cEpjzydSCq; start=\"<part1>\"")
      |> parse()

    assert Enum.count(params) == 2
    assert params["<part1>"]["start"] == true
    assert %Plug.Upload{} = file = params["<part1>"]["body"]
    assert File.read!(file.path) == "bar\n"
    assert file.content_type == "text/plain"
    assert file.filename == "foo.txt"
    assert params["<part2>"]["body"] == "foobar"
  end
end
